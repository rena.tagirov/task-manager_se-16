package ru.tagirov.tm;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tagirov.tm.configuration.SpringConfig;
import ru.tagirov.tm.init.Bootstrap;

public class Application {

    public static void main( String[] args ) throws Exception {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
        
    }
}
