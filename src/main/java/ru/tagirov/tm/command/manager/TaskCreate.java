package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.enumeration.Status;
import ru.tagirov.tm.util.DateUtil;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
public class TaskCreate extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "task create";
    }

    @Override
    public @NonNull String description() {
        return "create new task";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws ParseException, SQLException {
        System.out.println("[TASK CREATE]");
        @NotNull final User user = userSelect.getUser();
        @Nullable final List<Project> projects = new ArrayList<>(projectService.findAllByUserId(user.getId()));

        terminalService.printProjects(projects);
        System.out.println("WHICH PROJECT(0 - non project):");

        final int number = terminalService.parseInt();
        Project project = null;
        if (number < 0 || number > projects.size()) {
            System.out.println("Enter correct number!");
            return;
        } else if(number != 0)
            project = projects.get(number - 1);

        System.out.println("ENTER NAME:");
        @NotNull final String name = terminalService.readLine();

        System.out.println("ENTER DESCRIPTION");
        @NotNull final String description = terminalService.readLine();

        System.out.println("START TIME(DD-MM-YYYY):");
        @NotNull final Date dateBegin = DateUtil.getDate(terminalService.readLine());

        System.out.println("END TIME(DD-MM-YYYY):");
        @NotNull final Date dateEnd = DateUtil.getDate(terminalService.readLine());

        @NotNull final Task task = new Task();
        task.setId(UUID.randomUUID().toString());
        task.setName(name);
        task.setDescription(description);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        task.setProject(project);
        task.setUser(user);
        task.setStatus(Status.PLANNED);
        taskService.save(task);
        System.out.println("[OK]");
    }
}
