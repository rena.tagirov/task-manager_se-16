package ru.tagirov.tm.command.manager;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.sql.SQLException;

@Component
public class UserRemove extends AbstractCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "user remove";
    }

    @NotNull
    @Override
    public String description() {
        return "remove your account";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @NotNull
    @Override
    public Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws SQLException {
        System.out.println("[USER REMOVE]");

        @NotNull final User user = userSelect.getUser();

//        projectService.removeAllByUserId(user.getId());
//        taskService.removeAllByUserId(user.getId());
        userService.delete(user);
        System.out.println("[OK]\n");
    }
}
