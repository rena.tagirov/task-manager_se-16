package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.enumeration.Status;
import ru.tagirov.tm.util.DateUtil;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.UUID;

@Component
public class ProjectCreate extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "project create";
    }

    @Override
    public @NonNull String description() {
        return "create a new project";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws ParseException, SQLException {

        System.out.println("\n[PROJECT CREATE]");

        @NotNull final User user = userSelect.getUser();

        System.out.println("ENTER NAME:");
        @NotNull final String name = terminalService.readLine();

        System.out.println("ENTER DESCRIPTION");
        @NotNull final String description = terminalService.readLine();

        System.out.println("START TIME(DD-MM-YYYY):");
        @NotNull final Date dateBegin = DateUtil.getDate(terminalService.readLine());

        System.out.println("END TIME(DD-MM-YYYY):");
        @NotNull final Date dateEnd = DateUtil.getDate(terminalService.readLine());

        @NotNull final Project project = new Project();
        project.setId(UUID.randomUUID().toString());
        project.setName(name);
        project.setDescription(description);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        project.setUser(user);
        project.setStatus(Status.PLANNED);
        projectService.save(project);
        System.out.println("[OK]");
    }
}
