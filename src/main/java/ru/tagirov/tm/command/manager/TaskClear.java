package ru.tagirov.tm.command.manager;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.enumeration.Role;

import java.sql.SQLException;

@Component
public class TaskClear extends AbstractCommand {


    @Override
    public @NonNull String getCommand() {
        return "task clear";
    }

    @Override
    public @NonNull String description() {
        return "delete all tasks";
    }


    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws  SQLException {
        taskService.removeAllByUserId(userSelect.getUser().getId());
        System.out.println("[ALL TASK REMOVE]");
    }
}
