package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProjectClear extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "project clear";
    }

    @Override
    public @NonNull String description() {
        return "delete all projects";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws SQLException{
        System.out.println("\n[PROJECT CLEAR]");
        @NotNull final User user = userSelect.getUser();
        @Nullable final List<Project> projects = new ArrayList<>(projectService.findAllByUserId(user.getId()));

        if (projects.size() > 0) {
            projectService.removeAllByUserId(user.getId());
            System.out.println("[ALL PROJECT REMOVE]");
        }else {
            System.out.println("THE USER HAS NO PROJECTS");
        }
    }
}
