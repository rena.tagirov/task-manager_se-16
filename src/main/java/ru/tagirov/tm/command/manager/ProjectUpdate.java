package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.DateUtil;
import ru.tagirov.tm.util.StatusUtil;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProjectUpdate extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "project update";
    }

    @Override
    public @NonNull String description() {
        return "change project";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws ParseException, SQLException, IOException {
        System.out.println("[PROJECT UPDATE]");

        @NotNull final User user = userSelect.getUser();

        @NotNull final List<Project> projects = new ArrayList<>(projectService.findAllByUserId(user.getId()));

        terminalService.printProjects(projects);

        System.out.println("ENTER NUMBER:");
        final int number = terminalService.parseInt();

        if (number < 1 || number > projects.size()) {
            System.out.println("ENTER CORRECT NUMBER!");
            return;
        }

        for (int i = 0; i < projects.size(); i++) {
            if ((number - 1) == i) {
                projectService.save(selectField(projects.get(i)));
                break;
            }
        }

        System.out.println("[OK]");
    }

    @NotNull
    private Project selectField(@NotNull final Project project) throws ParseException{
        terminalService.printProjectField();

        @NotNull final String fieldNumber = terminalService.readLine();
        switch (fieldNumber) {
            case "1":
                System.out.println("Replace name to:");
                project.setName(terminalService.readLine());
                break;
            case "2":
                System.out.println("Replace description to:");
                project.setDescription(terminalService.readLine());
                break;
            case "3":
                System.out.println("Replace start date to:");
                project.setDateBegin(DateUtil.getDate(terminalService.readLine()));
                break;
            case "4":
                System.out.println("Replace end date to:");
                project.setDateEnd(DateUtil.getDate(terminalService.readLine()));
                break;
            case "5":
                System.out.println("Replace status to:");
                terminalService.printStatusField();
                project.setStatus(StatusUtil.changeStatus(terminalService.readLine()));
                break;
            default:
                System.out.println("Nothing has changed!");
                break;
        }

        return project;
    }
}
