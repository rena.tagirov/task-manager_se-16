package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class ProjectRemove extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "project remove";
    }

    @Override
    public @NonNull String description() {
        return "delete one project";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws SQLException {

        System.out.println("\n[PROJECT REMOVE]\nCHOOSE THE RIGHT USER: ");
        @NotNull final User user = userSelect.getUser();
        @NotNull final List<Project> projects = new ArrayList<>(Objects.requireNonNull(projectService.findAllByUserId(user.getId())));
        for (Project p : projects){
            System.out.println(p.getUser());
            System.out.println(p.getId());
        }

        terminalService.printProjects(projects);

        System.out.println("ENTER NUMBER:[0-Non-Project]");
        final int number = terminalService.parseInt();

        if (number < 0 || number > projects.size()) {
            System.out.println("Enter correct number!");
            return;
        }

        if (number == 0) {
            System.out.println("Non-project tasks remove");
            taskService.removeAllByIdAndProject_Id(user.getId(), null);
            System.out.println("[OK]");
            return;
        }

        for (int i = 0; i < projects.size(); i++) {
            if ((number - 1) == i) {
                projectService.removeByIdAndUser_Id(user.getId(), projects.get(i).getId());
                taskService.removeAllByIdAndProject_Id(user.getId(), projects.get(i).getId());
            }
        }

        System.out.println("[OK]\n");
    }
}
