package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.DateUtil;
import ru.tagirov.tm.util.StatusUtil;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class TaskUpdate extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "task update";
    }

    @Override
    public @NonNull String description() {
        return "update one task";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws IOException, ParseException, SQLException {
        System.out.println("[TASK UPDATE]");

        @NotNull final User user = userSelect.getUser();
        @NotNull final List<Task> tasks = new ArrayList<>(taskService.findAllByUserId(user.getId()));

        terminalService.printTasks(tasks);

        System.out.println("ENTER NUMBER:");
        final int number = terminalService.parseInt();

        if (number <= 0 || number > tasks.size()) {
            System.out.println("Enter correct number!");
            return;
        }

        for (int i = 0; i < tasks.size(); i++) {
            if (number - 1 == i) {
                taskService.save(selectField(tasks.get(i), user));
                break;
            }
        }

        System.out.println("[OK]");
    }

    @NotNull
    private Task selectField(@NotNull final Task task, User user) throws ParseException, IOException, SQLException {
        terminalService.printTaskField();

        @NotNull final String fieldNumber = terminalService.readLine();
        switch (fieldNumber) {
            case "1":
                System.out.println("Replace name to:");
                task.setName(terminalService.readLine());
                break;
            case "2":
                System.out.println("Replace description to:");
                task.setDescription(terminalService.readLine());
                break;
            case "3":
                System.out.println("Replace start date to:");
                task.setDateBegin(DateUtil.getDate(terminalService.readLine()));
                break;
            case "4":
                System.out.println("Replace end date to:");
                task.setDateEnd(DateUtil.getDate(terminalService.readLine()));
                break;
            case "5":
                System.out.println("Replace status to:");
                terminalService.printStatusField();
                task.setStatus(StatusUtil.changeStatus(terminalService.readLine()));
                break;
            case "6":
                System.out.println("Replace project to(number):");
                @Nullable final Project project = getProjectId(user);
                task.setProject(project);
                break;
            default:
                System.out.println("Nothing has changed!");
                break;
        }

        return task;
    }

    @Nullable
    private Project getProjectId(@Nullable final User user) throws SQLException {
        @NotNull final List<Project> projects = new ArrayList<>(projectService.findAllByUserId(user.getId()));
        terminalService.printProjects(projects);

        final int number = terminalService.parseInt();

        if (number == 0) {
            System.out.println("NON-PROJECT");
            return null;
        }

        if (number < 0 || number > projects.size()) {
            System.out.println("Enter correct number!");
            return null;
        }

        @Nullable Project project = null;
        for (int i = 0; i < projects.size(); i++) {
            if ((number - 1) == i)
                project = projects.get(i);
        }

        return Objects.requireNonNull(project);
    }
}
