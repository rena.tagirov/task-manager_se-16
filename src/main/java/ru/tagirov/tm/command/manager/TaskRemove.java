package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class TaskRemove extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "task remove";
    }

    @Override
    public @NonNull String description() {
        return "remove one task";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws SQLException {

        System.out.println("[TASK REMOVE]");
        @NotNull final User user = userSelect.getUser();
        @NotNull final List<Task> tasks  = new ArrayList<>(taskService.findAllByUserId(user.getId()));
        terminalService.printTasks(tasks);

        System.out.println("ENTER NUMBER:");
        final int number = terminalService.parseInt();

        if(number <= 0 || number > tasks.size()){
            System.out.println("Enter correct number!");
            return;
        }

        for (int i = 0; i < tasks.size(); i++) {
            if ((number - 1) == i) {
                taskService.removeByIdAndUser_Id(user.getId(), tasks.get(i).getId());
            }
        }

        System.out.println("[OK]");
    }
}