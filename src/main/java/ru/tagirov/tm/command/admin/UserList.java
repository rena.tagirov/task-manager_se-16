package ru.tagirov.tm.command.admin;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *  Класс команды UserList - выводит в консоль список всех пользователей.
 *  Команда для консоли - "user list".
 */

@Component
public class UserList extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "user list";
    }

    @Override
    public @NonNull String description() {
        return "show all users";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.ADMIN;
    }

    @Override
    public void execute() throws IOException, SQLException {

        System.out.println("\n[USER LIST]");
        @NotNull final List<User> users = new ArrayList<>(userService.findAll());

        for (User user : users) {
            System.out.println(user.toString());
        }
    }
}
