package ru.tagirov.tm.command;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tagirov.tm.api.service.ProjectService;
import ru.tagirov.tm.api.service.TaskService;
import ru.tagirov.tm.api.service.UserService;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.terminal.TerminalService;
import ru.tagirov.tm.util.UserSelect;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

public abstract class AbstractCommand{

    @Autowired
    protected UserService userService;

    @Autowired
    protected ProjectService projectService;

    @Autowired
    protected TaskService taskService;

    @Autowired
    protected TerminalService terminalService;

    @Autowired
    protected UserSelect userSelect;


    @NonNull
    public abstract String getCommand();

    @NonNull
    public abstract String description();

    public abstract boolean isSecure();

    @NotNull
    public abstract Role getRole();

    public abstract void execute() throws IOException, ParseException, NoSuchAlgorithmException, JAXBException, ClassNotFoundException, SQLException, InterruptedException;

}

