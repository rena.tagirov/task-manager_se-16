package ru.tagirov.tm.command.user;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;

@Component
public class UserShowProfile extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "show profile";
    }

    @Override
    public @NonNull String description() {
        return "show you profile";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException {

        @NotNull final User user = userService.getCurrentUser();

        System.out.println("User login: " + user.getLogin());
        System.out.println("User role: " + user.getRole().getValue());
    }
}

