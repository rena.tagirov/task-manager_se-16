package ru.tagirov.tm.command.user;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.sql.SQLException;

@Component
public class UserUpdateProfile extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "update login";
    }

    @Override
    public @NonNull String description() {
        return "update login";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, SQLException {

        @NotNull final User user = userService.getCurrentUser();

        System.out.println("[UPDATE LOGIN]");
        System.out.println("ENTER NEW LOGIN:");
        @NotNull final String login =terminalService.readLine();

        user.setLogin(login);
        userService.save(user);
        System.out.println("LOGIN IS EXIST!");
        System.out.println("[OK]");

    }
}

