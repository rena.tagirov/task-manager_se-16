package ru.tagirov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.sql.SQLException;

@Component
public class TaskSearch extends AbstractCommand {

    @Override
    public @NotNull String getCommand() {
        return "task find";
    }

    @Override
    public @NotNull String description() {
        return "Find task by name or description.";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, SQLException {
        System.out.println("\n[TASK FIND]");

        System.out.println("Find:");
        @NotNull final String text = terminalService.readLine();

//        @NotNull final List<Task> findTasks = new ArrayList<>(taskService.findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCase(text));
//
//        for (Task task : findTasks){
//            System.out.println(task.toString());
//        }
    }

}
