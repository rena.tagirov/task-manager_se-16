package ru.tagirov.tm.command.user;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.enumeration.PurposeComparator;
import ru.tagirov.tm.enumeration.Role;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProjectList extends AbstractCommand {

    @NotNull
    @Override
    public @NonNull String getCommand() {
        return "project list";
    }

    @NotNull
    @Override
    public @NonNull String description() {
        return "see all projects";
    }

    @Override
    public boolean isSecure() {
        return true;
    }


    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws SQLException {
        System.out.println("[PROJECT LIST]");

        @Nullable List<Project> projectList = null;

        if (userService.getCurrentUser().getRole() == Role.USER){
            projectList = getSortedList(new ArrayList<>(projectService.findAllByUserId(userService.getCurrentUser().getId())));
        }else {
            System.out.println("LIST OF ALL PROJECTS OR A SPECIFIC USER?\n"+ "1 - ALL\n" + "2 - SPECIFIC USER");
            String num = terminalService.readLine();
            if (num.equals("1")){
                projectList = getSortedList(new ArrayList<>(projectService.findAll()));
            }else if (num.equals("2")){
                projectList = getSortedList(new ArrayList<>(projectService.findAllByUserId(userSelect.getUser().getId())));
            }else {
                System.out.println("YOU ENTER INCORRECT NUMBER!");
            }
        }

        for (int i = 0; i < projectList.size(); i++) {
            @NotNull final Project project = projectList.get(i);
            System.out.println("Project #" + (projectList.get(i + 1)) + ".");
            System.out.println(project.toString());
        }
    }

    @NotNull
    private List<Project> getSortedList(@NotNull final List<Project> projectList) {
        terminalService.printHowSorted();
        switch (terminalService.readLine()) {
            case "1":
                projectList.sort(PurposeComparator.DATE_BEGIN_COMPARATOR.getComparator());
                break;
            case "2":
                projectList.sort(PurposeComparator.DATE_END_COMPARATOR.getComparator());
                break;
            case "3":
                projectList.sort(PurposeComparator.STATUS_COMPARATOR.getComparator());
                break;
            default:
                System.out.println("Insertion order!");
        }
        return projectList;
    }
}
