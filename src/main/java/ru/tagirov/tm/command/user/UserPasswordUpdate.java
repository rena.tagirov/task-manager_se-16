package ru.tagirov.tm.command.user;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.Md5Util;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

@Component
public class UserPasswordUpdate extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "password update";
    }

    @Override
    public @NonNull String description() {
        return "update you password";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, NoSuchAlgorithmException, SQLException {

        @NotNull final User user = userService.getCurrentUser();

        System.out.println("[USER PASSWORD UPDATE]");
        System.out.println("[ENTER OLD PASSWORD:]");
        @NotNull final String oldPass = terminalService.readLine();
        if (!user.getPassword().equals(Md5Util.getHash(oldPass))) {
            System.out.println("ENTER CORRECT PASSWORD!");
            return;
        }
        System.out.println("PASSWORD CORRECT!");
        System.out.println("[ENTER NEW PASSWORD:]");
        @NotNull final String newPass = terminalService.readLine();
        @NotNull final String newPassHash = Md5Util.getHash(newPass);

        user.setPassword(newPassHash);
        userService.save(user);
        System.out.println(user.getLogin() + " PASSWORD IS CHANGED!");
    }
}
