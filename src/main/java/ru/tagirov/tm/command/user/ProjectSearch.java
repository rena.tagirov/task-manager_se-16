package ru.tagirov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProjectSearch extends AbstractCommand {

    @Override
    public @NotNull String getCommand() {
        return "project find";
    }

    @Override
    public @NotNull String description() {
        return "Find project by name or description.";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, SQLException {
        System.out.println("\n[PROJECT FIND]");

        System.out.println("Find:");
        @NotNull final String text = terminalService.readLine();

        @NotNull final List<Project> findProjects = new ArrayList<>(projectService.findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCase(text));

        for (Project project : findProjects){
            System.out.println(project.toString());
        }
    }
}
