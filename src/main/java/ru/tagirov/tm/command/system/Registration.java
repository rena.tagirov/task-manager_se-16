package ru.tagirov.tm.command.system;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.Md5Util;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.UUID;

@Component
public class Registration extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "reg";
    }

    @Override
    public @NonNull String description() {
        return "registration";
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws  NoSuchAlgorithmException, SQLException {

        @Nullable final User userData;

        System.out.println("[USER REGISTRATION]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = terminalService.readLine();

        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = terminalService.readLine();
        @NotNull final String passwordHash = Md5Util.getHash(password);

        terminalService.printRoleField();
        @NotNull final String line = terminalService.readLine();
        @NotNull Role role;
        switch (line) {
            case "1":
                role = Role.ADMIN;
                break;
            case "2":
                role = Role.MANAGER;
                break;
            case "3":
                role = Role.USER;
                break;
            default:
                System.out.println("Enter correct role");
                return;
        }

        @NotNull final User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        user.setPassword(passwordHash);
        user.setRole(role);
        userService.save(user);
        System.out.println("[OK]\n");
    }
}
