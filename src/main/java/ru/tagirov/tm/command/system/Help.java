package ru.tagirov.tm.command.system;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.util.List;

@Component
public class Help extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "help";
    }

    @Override
    public @NonNull String description() {
        return "help";
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Autowired
    List<? extends AbstractCommand> abstractCommand;

    @Override
    public void execute() {

        @Nullable final User user = userService.getCurrentUser();
        for (@NotNull final AbstractCommand command : abstractCommand) {
            if (user == null && !command.isSecure()) {
                System.out.println(new StringBuilder().append(command.getCommand())
                        .append(": ")
                        .append(command.description()));
                continue;
            }
            if (user != null && command.isSecure() && (command.getRole().getLvl() <= user.getRole().getLvl())) {
                System.out.println(new StringBuilder().append(command.getCommand())
                        .append(": ")
                        .append(command.description()));
            }
            if("help".equals(command.getCommand()) || "about".equals(command.getCommand())){
                System.out.println(new StringBuilder().append(command.getCommand())
                        .append(": ")
                        .append(command.description()));
            }
        }
    }
}
