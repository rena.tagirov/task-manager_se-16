package ru.tagirov.tm.command.system;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.entity.Domain;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataSave extends About {

    @Override
    public @NotNull String getCommand() {
        return "save";
    }

    @Override
    public @NotNull String description() {
        return "Save state.";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.ADMIN;
    }

    @Override
    public void execute() throws IOException, JAXBException, SQLException {
        System.out.println("[DATA SAVE]");
        terminalService.printSaveField();
        @NotNull final String line = terminalService.readLine();

        @NotNull final List<User> users = new ArrayList<>(userService.findAll());
        @NotNull final List<Project> projects = new ArrayList<>(projectService.findAll());
        @NotNull final List<Task> tasks = new ArrayList<>(taskService.findAll());

        @NotNull final Domain domain = new Domain();
        domain.setUsers(users);
        domain.setProjects(projects);
        domain.setTasks(tasks);

        switch (line) {
            case "1":
                saveBinaryDomain(domain);
                break;
            case "2":
                saveXMLDomain(domain);
                break;
            case "3":
                saveJSONDomain(domain);
                break;
            case "4":
                saveXMLJaxDomain(domain);
                break;
            case "5":
                saveJSONJaxDomain(domain);
                break;
            default:
                System.out.println("Select correct number!");
                return;
        }

        System.out.println("[OK]");
    }

    private void saveBinaryDomain(@NotNull final Domain domain) throws IOException {
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream("C:\\Users\\user\\Desktop\\taskmanager\\src\\main\\resources\\domain.out"));
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
    }

    private void saveXMLDomain(@NotNull final Domain domain) throws IOException {
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(new File("C:\\Users\\user\\Desktop\\taskmanager\\src\\main\\resources\\domain.xml"), domain);
    }

    private void saveJSONDomain(@NotNull final Domain domain) throws IOException {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File("C:\\Users\\user\\Desktop\\taskmanager\\src\\main\\resources\\domain.json"), domain);
    }

    private void saveXMLJaxDomain(@NotNull final Domain domain) throws JAXBException {
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, new File("C:\\Users\\user\\Desktop\\taskmanager\\src\\main\\resources\\domainB.xml"));
    }

    private void saveJSONJaxDomain(@NotNull final Domain domain) throws JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.marshal(domain, new File("C:\\Users\\user\\Desktop\\taskmanager\\src\\main\\resources\\domainB.json"));
    }
}
