package ru.tagirov.tm.command.system;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.entity.Domain;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;

@Component
public class DateDownload extends About {

    @NotNull
    @Override
    public String getCommand() {
        return "download";
    }

    @NotNull
    @Override
    public String description() {
        return "Load state.";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @NotNull
    @Override
    public Role getRole() {
        return Role.ADMIN;
    }


    @Override
    public void execute() throws IOException, ClassNotFoundException, JAXBException, SQLException {
        System.out.println("[DATA LOAD]");
        terminalService.printLoadField();
        @NotNull final String line = terminalService.readLine();

        @NotNull final Domain domain;
        switch (line) {
            case "1":
                domain = loadBinaryDomain();
                break;
            case "2":
                domain = loadXMLDomain();
                break;
            case "3":
                domain = loadJSONDomain();
                break;
            case "4":
                domain = loadXMLJaxDomain();
                break;
            case "5":
                domain = loadJSONJaxDomain();
                break;
            default:
                System.out.println("Select correct number!");
                return;
        }

        userService.deleteAll();
        projectService.removeAll();
        taskService.deleteAll();

        @NotNull final List<User> users = domain.getUsers();
        @NotNull final List<Project> projects = domain.getProjects();
        @NotNull final List<Task> tasks = domain.getTasks();

        for (@NotNull final User user : users) {
            userService.save(user);
        }

        for (@NotNull final Project project : projects) {
            projectService.save(project);
        }

        for (@NotNull final Task task : tasks){
            taskService.save(task);
        }

        System.out.println("[OK]");
    }

    @NotNull
    private Domain loadBinaryDomain() throws IOException, ClassNotFoundException {
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("C:\\Users\\user\\Desktop\\taskmanager\\src\\main\\resources\\domain.out"));
        @NotNull final Object object =  objectInputStream.readObject();
        @NotNull final Domain domain = (Domain) object;
        objectInputStream.close();
        return domain;
    }

    @NotNull
    private Domain loadXMLDomain() throws IOException {
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String readContent = new String(Files.readAllBytes(Paths.get("C:\\Users\\user\\Desktop\\taskmanager\\src\\main\\resources\\domain.xml")));
        return xmlMapper.readValue(readContent, Domain.class);
    }

    private Domain loadJSONDomain() throws IOException {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String readContent = new String(Files.readAllBytes(Paths.get("C:\\Users\\user\\Desktop\\taskmanager\\src\\main\\resources\\domain.json")));
        return objectMapper.readValue(readContent, Domain.class);
    }

    private Domain loadXMLJaxDomain() throws JAXBException {
        @NotNull final File file = new File("C:\\Users\\user\\Desktop\\taskmanager\\src\\main\\resources\\domainB.xml");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return (Domain) unmarshaller.unmarshal(file);
    }

    private Domain loadJSONJaxDomain() throws JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final File file = new File("C:\\Users\\user\\Desktop\\taskmanager\\src\\main\\resources\\domainB.json");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        return (Domain) unmarshaller.unmarshal(file);
    }
}
