package ru.tagirov.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.enumeration.Role;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.sql.SQLException;

@Component
public class About extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "about";
    }

    @Override
    public @NonNull String description() {
        return "program description";
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, JAXBException, ClassNotFoundException, SQLException {
        System.out.println("Build Number: " + Manifests.read("buildNumber"));
        System.out.println("Developer: " + Manifests.read("developer"));
    }
}
