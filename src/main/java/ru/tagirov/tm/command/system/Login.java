package ru.tagirov.tm.command.system;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.Md5Util;

@Component
public class Login extends AbstractCommand {

    @Override
    public @NonNull String getCommand() {
        return "login";
    }

    @Override
    public @NonNull String description() {
        return "log into your account";
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute(){

        System.out.println("[USER AUTH]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = terminalService.readLine();

        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = terminalService.readLine();
        @NotNull final String passwordHash = Md5Util.getHash(password);

        @Nullable final User user = userService.findByLogin(login);
        if (user != null && user.getPassword().equals(passwordHash)) {
            userService.setCurrentUser(user);
            System.out.println("[OK]\n");
        }else {
            System.out.println("Incorrect user or password!");
        }
    }

}
