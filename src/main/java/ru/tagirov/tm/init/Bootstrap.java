package ru.tagirov.tm.init;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.api.service.UserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.terminal.TerminalService;
import ru.tagirov.tm.util.EntityCreateUtil;

import java.util.List;

@Component
public class Bootstrap {

    @Autowired
    private UserService userService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    List<? extends AbstractCommand> abstractCommands;

    @Autowired
    EntityCreateUtil entityCreateUtil;

    public void start() throws Exception {
        entityCreateUtil.create();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("\"help\" - get access command.");
        System.out.println();
        @Nullable String terminalCommand;

        while (true) {
            terminalCommand = terminalService.readLine();
            if ("EXIT".equals(terminalCommand))
                return;
            try {
                execute(terminalCommand);
            } catch (Exception e) {
                System.out.println(e.getMessage());
//                e.printStackTrace();
            }
        }
    }

    private void execute(@NotNull final String terminalCommand) throws Exception {
        AbstractCommand abstractCommand = abstractCommands.stream()
                .filter(x -> x.getCommand().equals(terminalCommand))
                .findFirst()
                .orElse(null);
        if (abstractCommand == null) {
            System.out.println("No found command!");
            return;
        }
        if (userService.getCurrentUser() == null && !abstractCommand.isSecure()) {
            abstractCommand.execute();
            return;
        }
        if (userService.getCurrentUser() != null && abstractCommand.isSecure()
                && (abstractCommand.getRole().getLvl() <= userService.getCurrentUser().getRole().getLvl())) {
            abstractCommand.execute();
            return;
        }
        if (abstractCommand.getCommand().equals("help")) {
            abstractCommand.execute();
        }
        System.out.println("No such command!");
    }

}