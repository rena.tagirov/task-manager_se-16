package ru.tagirov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tagirov.tm.entity.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    @NotNull
    List<Task> findAllByUserId(@NotNull final String userId);

    @Nullable
    void removeByIdAndUser_Id(@NotNull final String userId, @NotNull final String uuid);

    void removeAllByUserId(@NotNull final String userId);

    void removeAllByIdAndProject_Id(@NotNull final String id, @Nullable final String projectId);

//    List<Task> findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCase(@NotNull String text);

}
