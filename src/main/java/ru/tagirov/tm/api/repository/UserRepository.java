package ru.tagirov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tagirov.tm.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findByLogin(@NotNull String login);
}
