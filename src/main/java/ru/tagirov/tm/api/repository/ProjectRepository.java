package ru.tagirov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tagirov.tm.entity.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    @Nullable
    List<Project> findAllByUserId(@NotNull final String userId);

    void removeByIdAndUser_Id(@NotNull final String id, @NotNull final String userId);

    void removeAllByUserId(@NotNull final String userId);

//    Поиск по названию и описанию
    List<Project> findByNameContainingIgnoreCase(@NotNull String text);

}
