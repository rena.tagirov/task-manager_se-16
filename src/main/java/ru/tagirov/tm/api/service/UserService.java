package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.User;

import java.sql.SQLException;
import java.util.List;


public interface UserService {

    //    CRUD ----------------------------------------------------------------------

    void save(@NotNull final User user) throws SQLException;

    @NotNull
    List<User> findAll() throws SQLException;

    void delete(@NotNull final User user) throws SQLException;

    void deleteAll() throws SQLException;

    //    USER -----------------------------------------------------------------------


    @Nullable
    User getCurrentUser();

    void setCurrentUser(@Nullable User user);

    //    ALL ------------------------------------------------------------------------

    User findByLogin(@NotNull String login);
}
