package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.Task;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;


public interface TaskService {

    //    CRUD ----------------------------------------------------------------------

    void save(@NotNull final Task task);

    @Nullable
    List<Task> findAll();

    @Nullable
    void delete(@NotNull final Task task);

    void deleteAll();

    //    ALL ------------------------------------------------------------------------

    @NotNull
    Collection<Task> findAllByUserId(@NotNull final String userId);

    @Nullable
    void removeByIdAndUser_Id(@NotNull final String userId, @NotNull final String uuid);

    void removeAllByUserId(@NotNull final String userId);

    void removeAllByIdAndProject_Id(@NotNull final String userId, @Nullable final String projectId);

//    List<Task> findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCase(@org.jetbrains.annotations.NotNull String text);

}
