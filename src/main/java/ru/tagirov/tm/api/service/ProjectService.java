package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.Project;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;


public interface ProjectService {

    //    CRUD ----------------------------------------------------------------------

    void save(@NotNull final Project project) throws SQLException;

    @Nullable
    Project findOne(@NotNull final Project project) throws SQLException;

    @NotNull
    List<Project> findAll() throws SQLException;

    void remove(@NotNull final Project project) throws SQLException;

    void removeAll() throws SQLException;

    //    ALL ------------------------------------------------------------------------

    @Nullable
    Collection<Project> findAllByUserId(@NotNull final String userId) throws SQLException;

    void removeByIdAndUser_Id(@NotNull final String userId, @NotNull final String uuid) throws SQLException;

    void removeAllByUserId(@NotNull final String userId) throws SQLException;

    List<Project> findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCase(@NotNull String text);

}
