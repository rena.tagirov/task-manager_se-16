package ru.tagirov.tm.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.api.service.ProjectService;
import ru.tagirov.tm.api.service.TaskService;
import ru.tagirov.tm.api.service.UserService;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.enumeration.Status;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class EntityCreateUtil {

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private TaskService taskService;

    public void create() throws SQLException, ParseException {

        List<User> users = new ArrayList<>();
        users.add(new User(UUID.randomUUID().toString(), "a", Md5Util.getHash("a"), Role.ADMIN));
        users.add(new User(UUID.randomUUID().toString(), "name1", Md5Util.getHash("n1"), Role.USER));
        users.add(new User(UUID.randomUUID().toString(), "name2", Md5Util.getHash("n2"), Role.USER));
        users.add(new User(UUID.randomUUID().toString(), "name3", Md5Util.getHash("n3"), Role.USER));
        for (User u: users){
            userService.save(u);
        }

        List<Project> projects = new ArrayList<>();
        projects.add(new Project(UUID.randomUUID().toString(), "pr_name1_u1", "pr_dis1_u1",DateUtil.getDate("11-11-2012"), DateUtil.getDate("11-12-2012"),users.get(1), Status.PLANNED));
        projects.add(new Project(UUID.randomUUID().toString(), "pr_name2_u1", "pr_dis2_u1",DateUtil.getDate("12-11-2013"), DateUtil.getDate("12-12-2013"),users.get(1), Status.PLANNED));
        projects.add(new Project(UUID.randomUUID().toString(), "pr_name3_u1", "pr_dis3_u1",DateUtil.getDate("13-11-2014"), DateUtil.getDate("13-12-2014"),users.get(1), Status.PLANNED));
        projects.add(new Project(UUID.randomUUID().toString(), "pr_name4_u2", "pr_dis4_u2",DateUtil.getDate("14-11-2015"), DateUtil.getDate("14-12-2015"),users.get(2), Status.PLANNED));
        projects.add(new Project(UUID.randomUUID().toString(), "pr_name5_u2", "pr_dis5_u2",DateUtil.getDate("15-11-2016"), DateUtil.getDate("15-12-2016"),users.get(2), Status.PLANNED));
        projects.add(new Project(UUID.randomUUID().toString(), "pr_name6_u2", "pr_dis6_u2",DateUtil.getDate("16-11-2017"), DateUtil.getDate("16-12-2017"),users.get(2), Status.PLANNED));
        projects.add(new Project(UUID.randomUUID().toString(), "pr_name7_u3", "pr_dis7_u3",DateUtil.getDate("17-11-2019"), DateUtil.getDate("17-12-2019"),users.get(3), Status.PLANNED));
        projects.add(new Project(UUID.randomUUID().toString(), "pr_name8_u3", "pr_dis8_u3",DateUtil.getDate("18-11-2019"), DateUtil.getDate("18-12-2019"),users.get(3), Status.PLANNED));
        projects.add(new Project(UUID.randomUUID().toString(), "pr_name9_u3", "pr_dis9_u3",DateUtil.getDate("19-11-2020"), DateUtil.getDate("19-12-2020"),users.get(3), Status.PLANNED));
        for (Project p: projects){
            projectService.save(p);
        }

        List<Task> tasks = new ArrayList<>();
        tasks.add(new Task(UUID.randomUUID().toString(), "t_name1_u1", "t_dis1_u1",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(0),users.get(1), Status.PLANNED));
        tasks.add(new Task(UUID.randomUUID().toString(), "t_name2_u1", "t_dis2_u1",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(1),users.get(1), Status.PLANNED));
        tasks.add(new Task(UUID.randomUUID().toString(), "t_name3_u1", "t_dis3_u1",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(2),users.get(1), Status.PLANNED));
        tasks.add(new Task(UUID.randomUUID().toString(), "t_name4_u2", "t_dis4_u2",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(3),users.get(2), Status.PLANNED));
        tasks.add(new Task(UUID.randomUUID().toString(), "t_name5_u2", "t_dis5_u2",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(4),users.get(2), Status.PLANNED));
        tasks.add(new Task(UUID.randomUUID().toString(), "t_name6_u2", "t_dis6_u2",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(5),users.get(2), Status.PLANNED));
        tasks.add(new Task(UUID.randomUUID().toString(), "t_name7_u3", "t_dis7_u3",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(6),users.get(3), Status.PLANNED));
        tasks.add(new Task(UUID.randomUUID().toString(), "t_name8_u3", "t_dis8_u3",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(7),users.get(3), Status.PLANNED));
        tasks.add(new Task(UUID.randomUUID().toString(), "t_name9_u3", "t_dis9_u3",DateUtil.getDate("11-11-2020"), DateUtil.getDate("11-11-2020"),projects.get(8),users.get(3), Status.PLANNED));
        for (Task t: tasks){
            taskService.save(t);
        }
    }
}
