package ru.tagirov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.enumeration.Status;

public final class StatusUtil {

    public static @NotNull Status changeStatus(@NotNull final String line) {
        Status purposeStatus = null;
        switch (line) {
            case "1":
                purposeStatus = Status.PLANNED;
                break;
            case "2":
                purposeStatus = Status.DURING;
                break;
            case "3":
                purposeStatus = Status.READY;
                break;
            default:
                System.out.println("Status don't changed!");
        }

        return purposeStatus;
    }

}
