package ru.tagirov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tagirov.tm.api.repository.TaskRepository;
import ru.tagirov.tm.api.service.TaskService;
import ru.tagirov.tm.entity.Task;

import java.util.List;

@Service
@Transactional
public class TaskServiceImpl implements TaskService {


    @Autowired
    TaskRepository taskRepository;

    //    CRUD ----------------------------------------------------------------------

    @Override
    public void save(@NotNull Task task){
        taskRepository.save(task);
    }

    @Override
    public @NotNull List<Task> findAll(){
        return taskRepository.findAll();
    }

    @Override
    public void delete(@NotNull final Task task){
        taskRepository.delete(task);
    }

    @Override
    public void deleteAll(){
        taskRepository.deleteAll();
    }

    //    ALL ------------------------------------------------------------------------

    @Override
    public @NotNull List<Task> findAllByUserId(@NotNull String userId){
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public void removeByIdAndUser_Id(@NotNull String userId, @NotNull String uuid){
        taskRepository.removeByIdAndUser_Id(userId, uuid);
    }

    @Override
    public void removeAllByUserId(@NotNull String userId){
        taskRepository.removeAllByUserId(userId);
    }


    @Override
    public void removeAllByIdAndProject_Id(@NotNull String userId, @Nullable String projectId){
        taskRepository.removeAllByIdAndProject_Id(userId, projectId);
    }

//    @Override
//    public List<Task> findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCase(@NotNull String text) {
//        return taskRepository.findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCase(text);
//    }

}
