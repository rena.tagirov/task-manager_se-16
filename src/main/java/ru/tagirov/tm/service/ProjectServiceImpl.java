package ru.tagirov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tagirov.tm.api.repository.ProjectRepository;
import ru.tagirov.tm.api.service.ProjectService;
import ru.tagirov.tm.entity.Project;

import java.util.List;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    ProjectRepository projectRepository;

    //    CRUD ----------------------------------------------------------------------

    @Override
    public void save(@NotNull Project project){
        projectRepository.save(project);
    }

    @Override
    public @Nullable Project findOne(@NotNull final Project project){
        return projectRepository.getOne(project.getId());
    }

    @Override
    public @NotNull List<Project> findAll(){
        return  projectRepository.findAll();
    }

    @Override
    public void remove(@NotNull final Project project){
        projectRepository.delete(project);
    }

    @Override
    public void removeAll(){
        projectRepository.deleteAll();
    }

    //    ALL ------------------------------------------------------------------------

    @Override
    public @Nullable List<Project> findAllByUserId(@NotNull String userId){
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void removeByIdAndUser_Id(@NotNull String userId, @NotNull String uuid){
        projectRepository.removeByIdAndUser_Id(userId, uuid);

    }

    @Override
    public void removeAllByUserId(@NotNull String userId){
        projectRepository.removeAllByUserId(userId);
    }

    @Override
    public List<Project> findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCase(@NotNull String text) {
        return projectRepository.findByNameContainingIgnoreCase(text);
    }
}

