package ru.tagirov.tm.terminal;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

@Component
public class TerminalService {

    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @NonNull
    public String readLine(){
        String read = "";
        try {
            read = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return read;
    }

    public int parseInt(){
        int read = 0;
        try {
            read = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return read;
    }

    public void printRoleField(){
        System.out.println(new StringBuilder().append("ENTER ROLE:")
                .append("\n    1. admin")
                .append("\n    2. manager")
                .append("\n    3. user"));
    }

    public void printProjects(@NotNull final List<Project> projects) {
        int count = 0;
        for (Project project : projects) {
            System.out.println(++count + ". " + project.getName());
        }
    }

    public void printTasks(@NotNull final List<Task> tasks) {
        int count = 0;
        for (Task task : tasks) {
            System.out.println(++count + ". " + task.getName());
        }
    }

    public void printProjectField() {
        System.out.println(new StringBuilder().append("What do you want to change? ")
                .append("\n 1. Name")
                .append("\n 2. Description")
                .append("\n 3. Start date")
                .append("\n 4. End date")
                .append("\n 5. Status"));

    }

    public void printTaskField() {
        System.out.println(new StringBuilder().append("What do you want to change? ")
                .append("\n 1. Name")
                .append("\n 2. Description")
                .append("\n 3. Start date")
                .append("\n 4. End date")
                .append("\n 5. Status")
                .append("\n 6. Project"));

    }

    public void printStatusField(){
        System.out.println(new StringBuilder().append("Select status:")
                .append("\n 1.planned")
                .append("\n 2.during")
                .append("\n 3.ready"));
    }

    public void printHowSorted() {
        System.out.println(new StringBuilder()
                .append("How sorted:")
                .append("\n 1.Start date")
                .append("\n 2.End date")
                .append("\n 3.Status"));
    }

    public void printSaveField() {
        System.out.println(new StringBuilder()
                .append("How to save:")
                .append("\n1. Binary")
                .append("\n2. XML")
                .append("\n3. JSON")
                .append("\n4. XML-Jaxb")
                .append("\n5. JSON-Jaxb"));
    }

    public void printLoadField() {
        System.out.println(new StringBuilder()
                .append("How to load:")
                .append("\n1. Binary")
                .append("\n2. XML")
                .append("\n3. JSON")
                .append("\n4. XML-Jaxb")
                .append("\n5. JSON-Jaxb"));
    }

}
