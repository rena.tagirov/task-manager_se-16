package ru.tagirov.tm.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ComponentScan("ru.tagirov.tm")
@EnableJpaRepositories("ru.tagirov.tm.api.repository")
public class SpringConfig {

    @Bean
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/taskmanager");
        dataSource.setUsername("postgres");
        dataSource.setPassword("5285469");
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(final DataSource dataSource) {
        String service = "ru.tagirov.tm.service";
        String commandAdmin = "ru.tagirov.tm.service.command.admin";
        String commandManager = "ru.tagirov.tm.service.command.manager";
        String commandSystem = "ru.tagirov.tm.service.command.system";
        String commandUser = "ru.tagirov.tm.service.command.user";
        String commandBootstrap = "ru.tagirov.tm.service.init";
        String commandTerminal = "ru.tagirov.tm.service.terminal";
        String commandUtil = "ru.tagirov.tm.service.util";
        String commandEntity = "ru.tagirov.tm.entity";


        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
//        factoryBean.setPackagesToScan(service,
//                commandAdmin,
//                commandManager,
//                commandSystem,
//                commandUser,
//                commandBootstrap,
//                commandTerminal,
//                commandUtil,
//                commandEntity);
        factoryBean.setPackagesToScan("ru.tagirov.tm");
        final Properties properties = new Properties();
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "create");
        properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQL95Dialect");
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(final LocalContainerEntityManagerFactoryBean emf) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf.getObject());
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
        return new PersistenceExceptionTranslationPostProcessor();
    }

}
