package ru.tagirov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
@NoArgsConstructor
@Getter
@Setter
public class Domain implements Serializable {

    private static final long serialVersionUID = 6365028104735235988L;

    @NotNull
    private List<Project> projects = new ArrayList<>();

    @NotNull private List<Task> tasks = new ArrayList<>();

    @NotNull private List<User> users = new ArrayList<>();

}
