package ru.tagirov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.enumeration.Status;
import ru.tagirov.tm.util.DateUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "app_task")
public class Task extends PurposeEntity implements Serializable {

    private static final long serialVersionUID = 403527600450344261L;

    @ManyToOne(fetch = FetchType.EAGER)
    @Nullable
    private Project project;

    public Task(@NonNull final String id,
                @NonNull final String name,
                @NonNull final String description,
                @NonNull final Date dateBegin,
                @NonNull final Date dateEnd,
                @Nullable final Project project,
                @NonNull final User user,
                @NotNull final Status status) {
        super(id, name, description, dateBegin, dateEnd, user, status);
        this.project = project;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder()
                .append("\nName: ").append(getName())
                .append("\nDescription: ").append(getDescription())
                .append("\nStart date: ").append(DateUtil.getDate(getDateBegin()))
                .append("\nEnd date: ").append(DateUtil.getDate(getDateEnd()));
        if (project != null) {
            builder.append("\nProject Name: ").append(project.getName());
        }
        builder.append("\n--------------------------------------------------");
        return builder.toString();
    }
}