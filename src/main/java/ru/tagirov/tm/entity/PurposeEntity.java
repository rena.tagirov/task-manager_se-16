package ru.tagirov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.enumeration.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@MappedSuperclass
public class PurposeEntity extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 4035276004503442617L;

    @Column(name = "name")
    @NonNull
    private String name;

    @Column(name = "description")
    @NonNull
    private String description;

    @Column(name = "date_create")
    @NonNull
    private Date dateBegin;

    @Column(name = "date_update")
    @Nullable
    private Date dateEnd;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status = Status.PLANNED;

    public PurposeEntity(@NotNull final String id,
                         @NotNull final String name,
                         @NotNull final String description,
                         @NotNull final Date dateBegin,
                         @NotNull final Date dateEnd,
                         @NotNull final User user,
                         @NotNull final Status status) {
        super(id);
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        this.user = user;
        this.status = status;
    }
}
