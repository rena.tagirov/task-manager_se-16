package ru.tagirov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.enumeration.Role;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "app_user")
public class User extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 4035276004503441123L;

    @Column(unique = true, nullable = false)
    @NotNull
    private String login;

    @Column(nullable = false)
    @NotNull
    private String password;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull
    private Role role;

    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private List<Project> projects = new ArrayList<>();

    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private List<Task> tasks = new ArrayList<>();


    public User(@NonNull final String id,
                @NonNull final String login,
                @NonNull final String password,
                @NonNull final Role role) {
        super(id);
        this.login = login;
        this.password = password;
        this.role = role;
    }

    @Override
    public String toString() {
         StringBuilder builder = new StringBuilder()
                .append("role - ")
                .append(getRole().getValue())
                .append(" | ")
                .append("name - ")
                .append(getLogin())
                .append("\n--------------------------------------------------");
         return builder.toString();
    }
}
