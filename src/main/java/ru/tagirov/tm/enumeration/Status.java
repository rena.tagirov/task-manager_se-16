package ru.tagirov.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Status {

    PLANNED("planned", 1),
    DURING("during", 2),
    READY("ready", 3);

    @NotNull
    private String status;

    private int weight;

    Status(@NotNull final String status, final int weight) {
        this.status = status;
        this.weight = weight;
    }

    @NotNull
    public String getStatus() {
        return status;
    }

    public int getWeight() {
        return weight;
    }

    public void setStatus(@NotNull final String status) {
        this.status = status;
    }

    public void setWeight(final int weight) {
        this.weight = weight;
    }
}